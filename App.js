import React from 'react';
import {View,ScrollView, AsyncStorage} from "react-native";

import Header from "./components/header";
import TaskList from './components/task-list';
import BtAdd from './components/button-add-task';
import MenuTask from './components/menu-task';
import TaskPrompt from './components/prompt';

import {TASK} from './model';



export default class App extends React.Component {
  
  constructor(props){
    super(props);
    this.state={
      tasks:[], 
      isMenuTaskVisible:false,
      isAddTaskVisible:false,
      isRenameTaskVisible:false, 
      currentTask:{}, 
      currentIndex:0};
  }
  componentWillMount(){
    //chargement data
    this._retrieveData();
  }
//persistance
_storeData = async () => {
  try {
    await AsyncStorage.setItem('@Jdonet:taskList', JSON.stringify(this.state.tasks));
  } catch (error) {
    // Error saving data
    console.log("Error saving data")
  }
}

_retrieveData = async () => {
  try {
    const allTasks = await AsyncStorage.getItem('@Jdonet:taskList');
    if (allTasks !== null) {
      // We have data!!
      this.setState({tasks: JSON.parse(allTasks)})
    }
   } catch (error) {
     // Error retrieving data
     console.log('Error retrieving data');
   }
}
//fin persistance
  toggleMenuTaskVisibility = (task,i) => {
    this.setState({ 
      isMenuTaskVisible : !this.state.isMenuTaskVisible,
      currentTask : task,
      currentIndex : i
    });
  }
  toggleRenameTaskVisibility = (task,i) => {

    this.setState({ 
      isRenameTaskVisible : !this.state.isRenameTaskVisible,
      currentTask : task,
      currentIndex : i
    });
  }
  deleteCurrentTask = () => {
    //suppression de l element courant
    const list = this.state.tasks;
    list.splice(this.state.currentIndex,1);
    //maj du state
    this.setState({ tasks:list, currentIndex:{}, currentTask:{} });
    //masquer modal
    this.toggleMenuTaskVisibility();
    //persistance
    this._storeData();
  }

  changeTaskStatus = () => {
    const task = this.state.currentTask;
    task.status = this.state.currentTask.status === TASK.todoStatus ? TASK.doneStatus : TASK.todoStatus ; 
    const list = this.state.tasks;

    list[this.state.currentIndex] = task;

    //maj du state
    this.setState({ tasks:list, currentIndex:{}, currentTask:{} });
    //masquer modal
    this.toggleMenuTaskVisibility();
    //persistance
    this._storeData();
  }
  showAddPrompt = () => {
    this.setState({ 
      isAddTaskVisible : true
    });
  }
  hideAddPrompt = () => {
    this.setState({ 
      isAddTaskVisible : false
    });
  }
  hideRenamePrompt = () => {
    this.setState({ 
      isRenameTaskVisible : false
    });
  }
  onAddTask = (value) => {
    //recuperation des taches
    const list = this.state.tasks;
    //ajout tache
    list.push({
      id: list.length+1,
      content: value,
      status: 'En cours'});
    this.setState({ tasks:list, currentIndex:{}, currentTask:{} });
    this.hideAddPrompt();
    //persistance
    this._storeData();

  }
  onRenameTask = (value) => {
    //recuperation des taches
    const list = this.state.tasks;
    //ajout tache
    list[this.state.currentIndex].content = value;
    this.setState({ tasks:list, currentIndex:{}, currentTask:{} });
    this.hideRenamePrompt();
    //persistance
    this._storeData();
  }
  render() {
    return (    
      <View style={{flex:1}}>
        <Header content="Liste des taches"/>
        <ScrollView>
          <TaskList 
            tasks={this.state.tasks} 
            onPressCallback={this.toggleMenuTaskVisibility}
            onLongPressCallback={this.toggleRenameTaskVisibility} />
        </ScrollView>
        <MenuTask 
          taskName={ typeof(this.state.currentTask) === 'undefined' ? "" : this.state.currentTask.content}
          isVisible={this.state.isMenuTaskVisible} 
          onDisapearCallback={this.toggleMenuTaskVisibility} 
          onDeleteCallback={this.deleteCurrentTask} 
          onChangeStatusCallback={this.changeTaskStatus}/>
        <BtAdd
        onAddTaskCallback={this.showAddPrompt}
         />
        <TaskPrompt 
          isVisible={this.state.isAddTaskVisible} 
          title="Ajouter une tache"
          value=""
          placeholder="Ex:Acheter du lait"
          onCancelCallback={this.hideAddPrompt} 
          onSubmitCallback={this.onAddTask}
        /> 
        <TaskPrompt 
          isVisible={this.state.isRenameTaskVisible} 
          title={ typeof(this.state.currentTask) === 'undefined' ? "" : "Renommer la tache " +this.state.currentTask.content}
          value={ typeof(this.state.currentTask) === 'undefined' ? "" : this.state.currentTask.content}
          placeholder="Ex:Acheter du lait"
          onCancelCallback={this.hideRenamePrompt} 
          onSubmitCallback={this.onRenameTask}
        />

      </View>
    );
  }
  
}

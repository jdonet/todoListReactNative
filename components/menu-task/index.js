import React from 'react';
import { View,Text } from 'react-native';
import { Button } from 'react-native-elements';
import Modal from 'react-native-modal';
import { style } from './style';

const MenuTask = ({taskName,isVisible,onDisapearCallback,onDeleteCallback,onChangeStatusCallback}) =>(
    <Modal 
        isVisible={isVisible}
        onBackdropPress={() => onDisapearCallback()} 
    /* animationIn={'zoomInOut'}
        animationOut={'zoomOutUp'}
        animationInTiming={1000.00}
        animationOutTiming={1000.00}
        backdropTransitionInTiming={1000}
        backdropTransitionOutTiming={1000}*/
    >
            <View style={style.modal}>
                <View style={style.textView}>
                    <Text>Que souhaitez-vous faire sur la tache "{taskName}"?</Text>
                </View>
                <View style={style.buttonView}>
                    <Button 
                        title="Supprimer"
                        onPress={()=> onDeleteCallback()}
                        buttonStyle={style.buttonDelete}
                    />
                    <Button 
                        title="Changer statut"
                        onPress={()=> onChangeStatusCallback()}
                        buttonStyle={style.buttonChangesStatus}
                    />
                </View>
            </View>
    </Modal>
        
);

export default MenuTask;

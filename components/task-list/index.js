import React from 'react';
import { List, ListItem, Badge } from 'react-native-elements';
import {TASK} from '../../model';
import {APP_COLORS} from '../../style/color';
import {style} from './style'
const TaskList = ({tasks, onPressCallback,onLongPressCallback}) =>(
    <List containerStyle={style.list}> 
        {
            tasks.map((task, i) => (
            <ListItem
                key={task.id}
                title={task.content}
                onPress = {() => onPressCallback(task,i)}
                onLongPress = {() => onLongPressCallback(task,i)}
                badge= {
                    {
                        element:(
                            <Badge value={task.status}
                            containerStyle={
                                task.status === TASK.todoStatus ? {backgroundColor:APP_COLORS.accent} : {backgroundColor:APP_COLORS.lightPrimaryColor}
                            } />
                        )
                    }
                }
            />
            ))
        }
    </List> 
);


export default TaskList;
      
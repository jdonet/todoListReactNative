import React from 'react';
import Prompt from 'rn-prompt';

const TaskPrompt = ({isVisible,title,value, placeholder, onCancelCallback,onSubmitCallback}) =>(

<Prompt
    title={title}
    placeholder={placeholder}
    defaultValue={value}
    visible={ isVisible }
    onCancel={ () => onCancelCallback() }
    onSubmit={ (value) => onSubmitCallback(value) }
 />
);


export default TaskPrompt;
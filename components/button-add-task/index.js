import React from 'react';
import ActionButton from 'react-native-action-button';
import {Icon} from 'react-native-elements';
import {APP_COLORS} from '../../style/color';

const BtAdd = ({tasks, onAddTaskCallback}) =>(
    <ActionButton
  buttonColor={APP_COLORS.primaryAction}
  renderIcon={() => renderIcon()}
  onPress={() => onAddTaskCallback()}
/>
);

function renderIcon(){
  return (<Icon color={APP_COLORS.primaryAction} name="add"/>)
}


export default BtAdd;
      